---
title: About
layout: page
---
![Profile Image]({% if site.external-image %}{{ site.picture }}{% else %}{{ site.url }}/{{ site.picture }}{% endif %})

<p>Hello Visitor, I am Aman Gondaliya.</p>

<p>I am a 3rd student pursuing B.Tech in Electronics and Communication Engineering from Sardar Vallabhbhai National Institute of Technology, Surat.
</p>

<p>I am interested in CyberSecurity. I am mainly Focusing into Cryptography and Cryptocurrencies. I am also actively participating in CTFs (Hacking Competitions).Apart from that, I love playing cricket and football, watching TV series, Reading Novels and Playing Games.</p>


<h2>Skills</h2>

<ul class="skill-list">
	<li>Cryptography & Cryptocurrency</li>
	<li>Network Security</li>
	<li>Web Development</li>
	<li>Web Security</li>
	<li>IOT</li>
	<li>Hardware Securtiy</li>
	<li>Programming(C,C++,python)</li>
	<li>Git</li>
	<li>Mathematics</li>
	<li>Puzzle Solving</li>
	<li>Graphic Designing</li>
	<li>MATLAB & Scilab</li>

	
</ul>

<h2>Projects</h2>

<ul>
	<li><a href="https://github.com/">Lorem Lorem</a></li>
	<li><a href="https://github.com/">Ipsum Dolor</a></li>
	<li><a href="https://github.com/">Dolor Lorem</a></li>
</ul>
